using System;
using System.Text.RegularExpressions;

namespace ParlamenterHelper.Utils
{
    public class RegexString : NonEmptyString
    {
        public RegexString ( string _name, string _pattern)
            :   base( _name )
        {
            this.m_pattern = _pattern;
        }

        protected override void checkValue ( string _value )
        {
            base.checkValue( _value );
            if ( ! Regex.IsMatch( _value, m_pattern ) )
                throw new ArgumentException( "Invalid format", paramName );
        }

        private readonly string m_pattern;
    }
}
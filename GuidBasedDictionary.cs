using System;
using System.Text;

namespace ParlamenterHelper.Utils
{

	public class GuidBasedDictionary < T >
		:	System.Collections.IEnumerable
			where T : Entity
			
	{
			public GuidBasedDictionary( string _entityName )
			{
				m_entityName = _entityName;
				m_data = new System.Collections.Generic.Dictionary<
						Guid
					,	T
				>();
			}

			public void addItem( T _item )
			{
				if ( hasItem( _item.id ) )
				{
					StringBuilder b = new StringBuilder();
					b.AppendFormat( "Duplicated {0}", m_entityName  );
					throw new ArgumentException( b.ToString() );
				}

				m_data.Add( _item.id, _item );
			}

			public void removeItem( Guid _id )
			{
				if ( !hasItem( _id ) )
				{
					StringBuilder b = new StringBuilder();
					b.AppendFormat( "Missing {0}", m_entityName  );
					throw new ArgumentException( b.ToString() );
				}

				m_data.Remove( _id );
			}

			public T getItem( Guid _id )
			{
				if ( !hasItem( _id ) )
				{
					StringBuilder b = new StringBuilder();
					b.AppendFormat( "Missing {0}", m_entityName  );
					throw new ArgumentException( b.ToString() );
				}

				return m_data[ _id ];
			}

			public T findItem( Guid _id )
			{
				if ( !hasItem( _id ) )
					return null;

				return m_data[ _id ];
			}

			public bool hasItem( Guid _id )
			{
				return m_data.ContainsKey( _id );
			}

			public void clearItems()
			{
				m_data.Clear();
			}
				
			public int count
			{
				get
				{
					return m_data.Count;
				}
			}

			public System.Collections.IEnumerator GetEnumerator()
        		{
           		 	foreach ( var it in m_data )
                			yield return it;
       	 		}

			private string m_entityName;

			private System.Collections.Generic.Dictionary<
					Guid
				,	T
			> m_data;
	}

}
using System;

namespace ParlamenterHelper.Model
{
	public class AssistantAccount
		:	Account
	{
	        public AssistantAccount (
				Guid _id
			,	string _name
			,	string _email
			,	string _password
		)
	            :   base(
					_id
				,	_name
				,	_email
				,	_password
			)
	        {
	        }	
	}
}
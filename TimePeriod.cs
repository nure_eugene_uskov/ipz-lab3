using System;
using System.Text;
namespace ParlamenterHelper.Utils
{
	public class TimePeriod
		:	Value< TimePeriod >
	{
		public TimePeriod(
				DateTime _startTime
			,	TimeSpan _duration
		)
		{
			m_startTime = _startTime;
			m_duration = _duration;
		}

		public DateTime startTime
		{
			get
			{
				return m_startTime;
			}
		}

		public DateTime endTime
		{
			get
			{
				return m_startTime.Add( duration );
			}
		}

		public TimeSpan duration
		{
			get
			{
				return m_duration;
			}
		}

		public bool consists( DateTime _time )
		{
			return
					( _time > startTime )
				&&	( _time < endTime );
		}

		public bool overlaps( TimePeriod _period )
		{
			return
					consists( _period.startTime )
				||	consists( _period.endTime )
				||	_period.consists( startTime )
				||	_period.consists( endTime );
		}

		public override string ToString()
		{
			StringBuilder b = new StringBuilder();
			Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();

			b.Append( tr.getTabs() );
			b.AppendFormat(  "Start time = {0}\n", startTime );

			b.Append( tr.getTabs() );
			b.AppendFormat(  "End time = {0}\n", endTime );
			
			return b.ToString();
		}
		
		protected override System.Collections.Generic.IEnumerable< object > getAttributesToIncludeInEqualityCheck ()
		{
			return new object[] { startTime, duration };
		}

		private readonly DateTime m_startTime;
		private readonly TimeSpan m_duration;
	}
}
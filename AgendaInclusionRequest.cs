using System;
using System.Text;
namespace ParlamenterHelper.Model
{
	public class AgendaInclusionRequest
		:	Utils.Entity
	{
		public AgendaInclusionRequest(
				Guid _id
			,	Law _law
			,	DeputyAccount _deputy
			,	string _description
		)
			:	base( _id )
		{
			m_law.data = _law;
			m_proposingDeputy.data = _deputy;
			m_description.data = _description;
		}	

		public Law law
		{
			get
			{
				return m_law.data;
			}
		}

		public DeputyAccount proposingDeputy
		{
			get
			{
				return m_proposingDeputy.data;
			}
		}

		public string description
		{
			get
			{
				return m_description.data;
			}
		}

		public override string ToString()
		{
			StringBuilder b = new StringBuilder();
			Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "ID = {0}\n", id );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Law = {0}\n", law );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Deputy = {0}\n", proposingDeputy );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Description = {0}\n", description );

			return b.ToString();
		}

		private readonly Utils.RequiredProperty< DeputyAccount > m_proposingDeputy
            		= new Utils.RequiredProperty< DeputyAccount >( "deputy" );

		private readonly Utils.RequiredProperty< Law > m_law
            		= new Utils.RequiredProperty< Law >( "law" );

		private readonly Utils.NonEmptyString m_description
			= new Utils.NonEmptyString( "description" );
	}
}
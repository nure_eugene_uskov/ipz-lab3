namespace ParlamenterHelper.Model
{
	public enum VoteOption
	{
			For
		,	Against
		,	Abstain
		,	DontVote
		,	Absent
	}
}

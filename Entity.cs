using System;

namespace ParlamenterHelper.Utils
{
    public abstract class Entity
    {
        public Guid id
	{
		get
		{
			return m_id;
		}
		private set
		{
			m_id = value;
		}
	}


        protected Entity ( Guid _id )
        {
            if ( _id == null )
                throw new ArgumentNullException( "id" );

            this.m_id = _id;
        }

        public override bool Equals ( object _obj )
        {
            if ( this == _obj )
                return true;

            if ( _obj == null || GetType() != _obj.GetType() )
                return false;

            var otherEntity = ( Entity ) _obj ;
            return id == otherEntity.id;
        }

        public override int GetHashCode ()
        {
            return id.GetHashCode();
        }

	private Guid m_id;
    }
}
using SpeechRequests = ParlamenterHelper.Utils.GuidBasedDictionary< ParlamenterHelper.Model.SpeechRequest >;
using AgendaInclusionRequests = ParlamenterHelper.Utils.GuidBasedDictionary< ParlamenterHelper.Model.AgendaInclusionRequest >;
using AgendaItems = System.Collections.Generic.Dictionary< ParlamenterHelper.Model.Law, ParlamenterHelper.Model.AgendaItem >;

using System;
using System.Text;
namespace ParlamenterHelper.Model
{
	public class SessionAgenda
		:	Utils.Entity
	{
		public SessionAgenda( Guid _id )
			:	base( _id )
		{
			m_items = new AgendaItems();
			m_requests = new AgendaInclusionRequests( "agenda inclusion request" );
		}

		public Law currentBill
		{
			get
			{
				if ( m_currentBill == null )
					throw new Exception();

				return m_currentBill;
			}
		}

		public AgendaInclusionRequest createAgendaInclusionRequest(
				Law _law
			,	DeputyAccount _deputy
			,	string _description
		)
		{
			if ( m_items.ContainsKey( _law ) )
				throw new ArgumentException();

			AgendaInclusionRequest newAgendaInclusionRequst =
				new AgendaInclusionRequest(
						Guid.NewGuid()
					,	_law
					,	_deputy
					,	_description
				);

			m_requests.addItem( newAgendaInclusionRequst );
			return newAgendaInclusionRequst;
		}

		public void removeInclusionRequest( AgendaInclusionRequest _request )
		{
			m_requests.removeItem( _request.id );
		}

		public void acceptInclusionRequest( AgendaInclusionRequest _request )
		{
			m_items.Add(  
					_request.law
				,	new AgendaItem(
							_request.law
						,	_request.proposingDeputy
						,	_request.description
					)
			 );
			_request.law.includedInAgenda = true;
		}

		public void removeAgendaItem( Law _law )
		{
			m_items.Remove( _law );

			_law.includedInAgenda = false;
		}

		public void changeCurrentBill( Law _law )
		{
			if ( !m_items.ContainsKey( _law ) )
				throw new ArgumentException();

			m_currentBill = _law;
		}

		public override string ToString()
		{
			StringBuilder b = new StringBuilder();
			Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "ID = {0}\n", id );

			b.Append( tr.getTabs() );
			b.AppendLine( "Items:" );
			tr.increaceTabNumber();
			foreach( var item in m_items )
			{
				b.Append( item );
				b.AppendLine();
			}
			tr.decreaceTabNumber();
			
			b.Append( tr.getTabs() );
			b.AppendLine( "Requests:" );
			tr.increaceTabNumber();
			foreach( var it in m_requests )
			{
				b.Append( it );
				b.AppendLine();
			}
			tr.decreaceTabNumber();
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Law = {0}\n", m_currentBill );
	
			b.Append( tr.getTabs() );
			b.AppendFormat( "Current bill = {0}\n", currentBill );

			return b.ToString();
		}

		private AgendaItems m_items;

		private AgendaInclusionRequests m_requests;

		private Law m_currentBill;
	}
}
using System;
using System.Text;
namespace ParlamenterHelper.Model
{
    public class MeetingRequest
	:	Utils.Entity
    {
	        public MeetingRequest (
				Guid _id
			,	Meeting _meeting
			,	DeputyAccount _invitedDeputy
		)
	            :   base( _id )
	        {
			m_meeting.data = _meeting;
			m_invitedDeputy.data = _invitedDeputy;
			m_status = RequestStatus.NoAnswer;
	        }

		public Meeting meeting
		{
			get
			{
				return m_meeting.data;
			}
		}

		public DeputyAccount invitedDeputy
		{
			get
			{
				return m_invitedDeputy.data;
			}
		}

		public RequestStatus status
		{
			set
			{
				m_status = value;
			}
			get
			{
				return m_status;
			}
		}
	
	        public override string ToString()
	        {		
			StringBuilder b = new StringBuilder();
			Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "ID = {0}\n", id );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Meeting subject = {0}\n", m_meeting.data.subject );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Invited deputy name = {0}\n", invitedDeputy.name );

			return b.ToString();
	        }

		private readonly Utils.RequiredProperty< Meeting > m_meeting
            		= new Utils.RequiredProperty< Meeting >( "meeting" );

		private readonly Utils.RequiredProperty< DeputyAccount > m_invitedDeputy
            		= new Utils.RequiredProperty< DeputyAccount >( "deputy" );

		RequestStatus m_status;
    }
}
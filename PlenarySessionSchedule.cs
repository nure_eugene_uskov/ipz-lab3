using System;
using System.Text;
using PlenarySessions = ParlamenterHelper.Utils.TimePeriodBasedList< ParlamenterHelper.Model.PlenarySession >;

namespace ParlamenterHelper.Model
{
public class PlenarySessionSchedule
	:	Utils.Entity
//	,	System.Collections.IEnumerable
{
	public PlenarySessionSchedule( Guid _id )
		:	base( _id )
	{
		m_sessions = new PlenarySessions();
	}

	public PlenarySession createSession( Utils.TimePeriod _timePeriod )
	{
		PlenarySession newSession = new PlenarySession(
				Guid.NewGuid()
			,	_timePeriod
		);

		m_sessions.addItem( newSession );
		return newSession;
	}

	public void removePlenarySession( Utils.TimePeriod _timePeriod )
	{
		m_sessions.removeItem( _timePeriod );
	}

	public System.Collections.IEnumerable sessions()
	{
		foreach( var it in m_sessions )
		{
			yield return it;
		}
	}

	public override string ToString()
	{
		StringBuilder b = new StringBuilder();
		Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();

		b.Append( tr.getTabs() );
		b.AppendFormat(  "ID = {0}\n", id );


		b.Append( tr.getTabs() );
		b.AppendLine( "Sessions:" );
		tr.increaceTabNumber();
		foreach( var it in m_sessions )
		{
			b.Append( tr.getTabs() );
			b.Append( it );
			b.AppendLine();
		}
		tr.decreaceTabNumber();

		return b.ToString();
	}


/*
	public System.Collections.IEnumerator GetEnumerator()
	{
		 	foreach ( var it in m_meetings )
			yield return it;
       	 }
*/

	private readonly PlenarySessions m_sessions;

}

}
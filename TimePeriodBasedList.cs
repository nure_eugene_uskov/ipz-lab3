using System;
namespace ParlamenterHelper.Utils
{

	public class TimePeriodBasedList< T >
		:	System.Collections.IEnumerable
			where T : ILastingEvent
	{
		public TimePeriodBasedList()
		{
			m_data = new System.Collections.Generic.SortedList< TimePeriod, T >();
		}

		public void addItem( T _item )
		{
			if ( !checkItem( _item ) )
				throw new ArgumentException( "invalid time period of item" );

			m_data.Add( _item.timePeriod, _item );
		}

		public void removeItem( TimePeriod _time )
		{
			if ( !m_data.ContainsKey( _time ) )
				throw new ArgumentException( "invalid time period of item" );

			m_data.Remove( _time );
		}

		public System.Collections.IEnumerator GetEnumerator()
		{
   		 	foreach ( var it in m_data )
       			yield return it;
		}

		private bool checkItem( T _item )
		{
			foreach( var it in m_data )
			{
				if ( _item.timePeriod.overlaps( it.Value.timePeriod ) )
					return false;
			}

			return true;
		}

		private readonly System.Collections.Generic.SortedList< TimePeriod, T > m_data;

	}

}
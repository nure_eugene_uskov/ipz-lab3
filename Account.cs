using System;
using System.Text;
using System.Text.RegularExpressions;

namespace ParlamenterHelper.Model
{
    public class Account : Utils.Entity
    {
        public string name
	{
        	get
		{
			return m_name.data;
		}
        	set
		{
			m_name.data = value;
		}
        }

        public string email
	{
        	 get
		{
			return m_email.data;
		}
        	set
		{
			m_email.data = value;
		}
        }


        public Account (
			Guid _id
		,	string _name
		,	string _email
		,	string _password
	)
            :   base( _id )
        {
            this.name = _name;
            this.email = _email;
            changePassword( _password );
        }

        public override string ToString()
        {
		StringBuilder b = new StringBuilder();
		Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();

		b.Append( tr.getTabs() );
		b.AppendFormat(  "ID = {0}\n", id );

		b.Append( tr.getTabs() );
		b.AppendFormat(  "Name = {0}\n", name );

		b.Append( tr.getTabs() );
		b.AppendFormat(  "Email = {0}\n", email );

		b.Append( tr.getTabs() );
		b.AppendFormat(  "Password = {0}\n", m_password.data );

		return b.ToString();
        }

        public bool checkPassword ( string _password )
        {
            if ( _password == null )
                throw new ArgumentNullException( "password" );

            return this.m_password.data == _password;
        }

        public void changePassword ( string _password )
        {
            this.m_password.data = _password;
        }

        private Utils.NonEmptyString m_name = new Utils.NonEmptyString( "name" );
        private Utils.RegexString m_email = new Utils.RegexString( "email", "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$" );
        private Utils.NonEmptyString m_password = new Utils.NonEmptyString( "password" );

    }
}
using System;
using System.Text;
using Meetings = ParlamenterHelper.Utils.TimePeriodBasedList< ParlamenterHelper.Model.Meeting >;

namespace ParlamenterHelper.Model
{
public class MeetingSchedule
	:	Utils.Entity
//	,	System.Collections.IEnumerable
{
	public MeetingSchedule(
			Guid _id
		,	DeputyAccount _owner
	)
		:	base( _id )
	{
		m_owner.data = _owner;
		m_meetings = new Meetings();
	}

	public DeputyAccount owner
	{
		get
		{
			return m_owner.data;
		}
	}
/*
	public Meeting createMeeting( 
			string _subject
		,	Utils.TimePeriod _timePeriod
	)
	{
		Meeting newMeeting = new Meeting(
				Guid.NewGuid()
			,	_subject
			,	owner
			,	_timePeriod
		);

		m_meetings.addItem( newMeeting );
		return newMeeting;
	}
*/
	public void addMeeting( Meeting _meeting )
	{
		m_meetings.addItem( _meeting );
	}

	public void removeMeeting( Meeting _meeting )
	{
		m_meetings.removeItem( _meeting.timePeriod );
	}

	public System.Collections.IEnumerable meetings()
	{
		foreach( var it in m_meetings )
		{
			yield return it;
		}
	}

	public override string ToString()
	{
		StringBuilder b = new StringBuilder();
		Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();

		b.Append( tr.getTabs() );
		b.AppendFormat(  "ID = {0}\n", id );


		b.Append( tr.getTabs() );
		b.AppendLine( "Meetings:" );
		tr.increaceTabNumber();
		foreach( var it in m_meetings )
		{
			b.Append( tr.getTabs() );
			b.Append( it );
			b.AppendLine();
		}
		tr.decreaceTabNumber();
		
		b.Append( tr.getTabs() );
		b.AppendFormat( "Deputy name = {0}\n", owner.name );

		return b.ToString();
	}

/*
	public System.Collections.IEnumerator GetEnumerator()
	{
		 	foreach ( var it in m_meetings )
			yield return it;
       	 }
*/
	private readonly Utils.RequiredProperty< DeputyAccount > m_owner
            	= new Utils.RequiredProperty< DeputyAccount >( "deputy" );

	private readonly Meetings m_meetings;

}

}
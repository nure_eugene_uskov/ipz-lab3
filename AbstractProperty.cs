using System;

namespace ParlamenterHelper.Utils
{
    public abstract class AbstractProperty
    {
        public string paramName
	{
		get
		{
			return m_paramName;
		}
		private set
		{
			m_paramName = value;
		}
	}

        protected AbstractProperty ( string _paramName )
        {
            if ( _paramName == null )
                throw new ArgumentNullException( "paramName" );

            this.m_paramName = _paramName;
        }

	private string m_paramName;
    }
}
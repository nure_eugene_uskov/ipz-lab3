using System;
using System.Text;
using SpeechRequests = System.Collections.Generic.Dictionary< ParlamenterHelper.Model.DeputyAccount, ParlamenterHelper.Model.SpeechRequest >;
using Votings = ParlamenterHelper.Utils.GuidBasedDictionary< ParlamenterHelper.Model.Voting >;
using Speeches = ParlamenterHelper.Utils.GuidBasedDictionary< ParlamenterHelper.Model.Speech >;
using Deputies = ParlamenterHelper.Utils.GuidBasedDictionary< ParlamenterHelper.Model.DeputyAccount >;

namespace ParlamenterHelper.Model
{
	public class PlenarySession
		:	Utils.Entity
		,	Utils.ILastingEvent
	{
		public PlenarySession(
				Guid _id
			,	Utils.TimePeriod _timePeriod
		)
			:	base( _id )
		{
			//m_items = new AgendaItems();
			m_speechRequests = new SpeechRequests();
			m_votings = new Votings( "voting" );
			m_speeches = new Speeches( "speech" );
			m_agenda.data = new SessionAgenda( Guid.NewGuid() );
			m_deputies = new Deputies( "deputy" );
			//m_agendaInclusionRequests = new AgendaInclusionRequests();
			m_timePeriod = _timePeriod;
		}

		public SpeechRequest createSpeechRequest(
				DeputyAccount _deputy
		)
		{
			if ( m_speechRequests.ContainsKey( _deputy ) )
				throw new ArgumentException();
			
			SpeechRequest newSpeechRequest = new SpeechRequest(
					Guid.NewGuid()
				,	_deputy
			);
			m_speechRequests.Add( _deputy, newSpeechRequest );
			return newSpeechRequest;
		}

		public void refuseSpeechRequest( SpeechRequest _request )
		{
			m_speechRequests.Remove( _request.speaker );
		}

		public void acceptSpeechRequest( SpeechRequest _request, DateTime _dateTime )
		{
			m_speechRequests.Remove( _request.speaker );

			Speech newSpeech = new Speech(
					Guid.NewGuid()
				,	_request.speaker
				,	agenda.currentBill
				,	_dateTime
			);
			m_speeches.addItem( newSpeech );
		}

		public Voting createVoting( DateTime _dateTime )
		{
			Voting newVoting = new Voting(
					Guid.NewGuid()
				,	this
				,	agenda.currentBill
				,	_dateTime
			);

			m_votings.addItem( newVoting );
			return newVoting;
		}

		public void registerDeputy( DeputyAccount _deputy )
		{
			m_deputies.addItem( _deputy );
		}
			
		public bool hasDeputy( DeputyAccount _deputy )
		{
			return m_deputies.hasItem( _deputy.id );
		}

		public void changeCurrentBill( Law _law )
		{
			m_speechRequests.Clear();
			agenda.changeCurrentBill( _law );
		}

		public SessionAgenda agenda
		{
			get
			{
				return m_agenda.data;
			}
		}

		public System.Collections.IEnumerable speechRequests()
		{
			foreach( var it in m_speechRequests )
			{
				yield return it;
			}
		}

		public System.Collections.IEnumerable speechs()
		{
			foreach( var it in m_speeches )
			{
				yield return it;
			}
		}

		public System.Collections.IEnumerable votings()
		{
			foreach( var it in m_votings )
			{
				yield return it;
			}
		}

		public System.Collections.IEnumerable deputies()
		{
			foreach( var it in m_deputies )
			{
				yield return it;
			}
		}
			
		public Utils.TimePeriod timePeriod
		{
			get
			{
				return m_timePeriod;
			}
		}
			
		public int totalDeputiesCount
		{
			get
			{
				return Resources.Resources.c_totalDeputiesCount;
			}
		}
			
		public int registeredDeputiesCount
		{
			get
			{
				return m_deputies.count;
			}
		}

		public override string ToString()
		{
			StringBuilder b = new StringBuilder();
			Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "ID = {0}\n", id );

			b.Append( tr.getTabs() );
			b.AppendLine( "Speech requests:" );
			tr.increaceTabNumber();
			foreach( var it in m_speechRequests )
			{
				b.Append( tr.getTabs() );
				b.Append( it );
				b.AppendLine();
			}
			tr.decreaceTabNumber();

			b.Append( tr.getTabs() );
			b.AppendLine( "Registered deputies:" );
			tr.increaceTabNumber();
			foreach( var it in m_deputies )
			{
				b.Append( tr.getTabs() );
				b.Append( it );
				b.AppendLine();
			}
			tr.decreaceTabNumber();

			b.Append( tr.getTabs() );
			b.AppendLine( "Votings:" );
			tr.increaceTabNumber();
			foreach( var it in m_votings )
			{
				b.Append( it );
				b.AppendLine();
			}
			tr.decreaceTabNumber();

			b.Append( tr.getTabs() );
			b.AppendLine( "Speeches:" );
			tr.increaceTabNumber();
			foreach( var it in m_speeches )
			{
				b.Append( tr.getTabs() );
				b.Append( it );
				b.AppendLine();
			}
			tr.decreaceTabNumber();
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Time period = {0}\n", timePeriod );

			return b.ToString();
		}

		//private AgendaItems m_items;
		
		private readonly SpeechRequests m_speechRequests;

		//private AgendaInclusionRequests m_agendaInclusionRequests;

		private readonly Utils.RequiredProperty< SessionAgenda > m_agenda
            		= new Utils.RequiredProperty< SessionAgenda >( "agenda" );

		private readonly Deputies m_deputies;

		private readonly Votings m_votings;

		private readonly Speeches m_speeches;
			
		private readonly Utils.TimePeriod m_timePeriod;
	}
}
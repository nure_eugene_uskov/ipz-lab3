using System;
using System.Text;
namespace ParlamenterHelper.Model
{
	public class SpeechRequest
		:	Utils.Entity
	{
		public SpeechRequest( Guid _id, DeputyAccount _deputy )
			:	base( _id )
		{
			m_speaker.data = _deputy;
		}
		
		public DeputyAccount speaker
		{
			get
			{	
				return m_speaker.data;
			}
		}
		
		public override string ToString()
		{		
			StringBuilder b = new StringBuilder();
			Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "ID = {0}\n", id );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Speacker = {0}\n", speaker );

			return b.ToString();
		}

		private readonly Utils.RequiredProperty< DeputyAccount > m_speaker
            = new Utils.RequiredProperty< DeputyAccount >( "deputy" );
	}
}
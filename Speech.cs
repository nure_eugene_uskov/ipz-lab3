using System;
using System.Text;
namespace ParlamenterHelper.Model
{

public class Speech
	:	Utils.Entity
{
	public Speech(
			Guid _id
		,	DeputyAccount _deputy
		,	Law _law 
		,	DateTime _dateTime
	)
		:	base( _id )
	{
		m_deputy.data = _deputy;
		m_law.data = _law;
		m_dateTime = _dateTime;
	}

	public DeputyAccount speacker
	{
		get
		{
			return m_deputy.data;
		}
	}

	public Law law
	{
		get
		{
			return m_law.data;
		}
	}

	public DateTime dateTime
	{
		get 
		{
			return m_dateTime;
		}
	}

	public override string ToString()
	{
		StringBuilder b = new StringBuilder();
		Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();

		b.Append( tr.getTabs() );
		b.AppendFormat(  "ID = {0}\n", id );

		b.Append( tr.getTabs() );
		b.AppendFormat(  "Deputy = {0}\n", speacker );

		b.Append( tr.getTabs() );
		b.AppendFormat(  "Law = {0}\n", law );

		b.Append( tr.getTabs() );
		b.AppendFormat(  "Time = {0}\n", dateTime );
		
		return b.ToString();
	}

	private readonly Utils.RequiredProperty< DeputyAccount > m_deputy
            	= new Utils.RequiredProperty< DeputyAccount >( "deputy" );

	private readonly Utils.RequiredProperty< Law > m_law
            	= new Utils.RequiredProperty< Law >( "law" );

	private DateTime m_dateTime;
	
}

}
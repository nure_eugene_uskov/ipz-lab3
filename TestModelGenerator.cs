using ParlamenterHelper.Model;

using System;


namespace ParlamenterHelper.TestApp
{
    static class TestModelGenerator
    {
        public static ParlamenterHelperModel GenerateTestData ()
        {
            ParlamenterHelperModel m = new ParlamenterHelperModel();

            GenerateAccounts( m );
            GenerateLaws( m );
            GenerateNotes( m );
            GenerateMeetings( m );
            GeneratePlenarySessions( m );
            GenerateAgendaItems( m );
            GenerateSpeeches( m );
            GenerateVotings( m );
			

            return m;
        }


        private static void GenerateAccounts ( ParlamenterHelperModel _m )
        {
		Account opertorAccount = new Account( Guid.NewGuid(), "Ivan Ivanov", "ivan.ivanov@parlamenthelper.com", "12345" );
		Account parlamentHier = new Account( Guid.NewGuid(), "John Johnov", "john.johnov@parlamenthelper.com", "543t1" );

		AssistantAccount assistantCrossby = new AssistantAccount( Guid.NewGuid(), "Crossby Galloway", "crossby.galloway@parlamenthelper.com", "11111" );
		AssistantAccount assistantCharles = new AssistantAccount( Guid.NewGuid(), "Charles Crane", "charles.crane@parlamenthelper.com", "54321" );
		

		Igor = new DeputyAccount( Guid.NewGuid(), "Igor Mclean", "igor.mclean@parlamenthelper.com", "34555" );
		Kohen = new DeputyAccount( Guid.NewGuid(), "Kohen Wheeler", "vasy.wheeler@parlamenthelper.com", "53355" );
		Kole = new DeputyAccount( Guid.NewGuid(), "Kole Welch", "kote.welch@parlamenthelper.com", "53311" );

		Kole.assignAssistant( assistantCrossby );
		Kole.assignAssistant( assistantCharles );


        _m.accounts.Add( opertorAccount );
		_m.accounts.Add( parlamentHier );
		_m.accounts.Add( assistantCrossby );
		_m.accounts.Add( assistantCharles );
		_m.accounts.Add( Igor );
		_m.accounts.Add( Kohen );
		_m.accounts.Add( Kole );
        }

        private static void GenerateLaws ( ParlamenterHelperModel _m )
        {
            // ----

		NoAlcoholLow = new Law ( Guid.NewGuid(), "No Alchohol Law", "@./resources/laws/l1", new DateTime( 2000, 7, 7 ) );
		CarryingWeaponProhibition = new Law( Guid.NewGuid(),"Carrying weapon is prohibited", "@./resources/laws/l2", new DateTime( 2001, 6, 6 ) );
		DrugProhibition = new Law(  Guid.NewGuid(),"Drugs are prohibited", "@./resources/laws/l3", new DateTime( 2002, 5, 5  ) );

		NoAlcoholLow.history.addItem( new LawHistoryItem( "@./resources/laws/l11", new DateTime( 2002, 10, 10 ), LawStatus.New ) );
		NoAlcoholLow.history.addItem( new LawHistoryItem( "@./resources/laws/l12", new DateTime( 2005, 11, 11 ), LawStatus.VotedIn1Reading ) );

		CarryingWeaponProhibition.history.addItem( new LawHistoryItem( "@./resources/laws/l21", new DateTime( 2005, 11, 10 ), LawStatus.VotedIn1Reading ) );
            // ----

            _m.laws.Add( NoAlcoholLow );
            _m.laws.Add( CarryingWeaponProhibition );
            _m.laws.Add( DrugProhibition );
        }



	  private static void GenerateMeetings ( ParlamenterHelperModel _m )
        {

            Meeting drugMeeting = new Meeting(
                    Guid.NewGuid()
                ,   "meeting about drug law"
                ,   Kole
                ,   new Utils.TimePeriod(
                            new DateTime( 2007, 11, 10 )
                        ,   new TimeSpan( 2, 0, 0 )
                    )
             );

            Kole.meetingSchedule.addMeeting( drugMeeting );

		//Meeting drugMeeting = Kole.meetingSchedule.createMeeting(  );
		
		MeetingRequest IgorRequest = drugMeeting.inviteDeputy( Igor );

        Igor.acceptMeetingRequest( IgorRequest );


            MeetingRequest KohenRequst = drugMeeting.inviteDeputy( Kohen );


            

            Meeting alcocholMeeting = new Meeting(
                    Guid.NewGuid()
                ,   "meeting about alchohol law"
                ,   Kole
                ,   new Utils.TimePeriod(
                            new DateTime( 2007, 11, 11 )
                        ,   new TimeSpan( 3, 0, 0 )
                    )
             );

            Kohen.meetingSchedule.addMeeting( alcocholMeeting );

		//Meeting alcocholMeeting = Kohen.meetingSchedule.createMeeting( "meeting about alchohol law", new Utils.TimePeriod( new DateTime( 2007, 11, 11 ), new TimeSpan( 3, 0, 0 ) ) );
         MeetingRequest KoleReuest = alcocholMeeting.inviteDeputy( Kole );
           MeetingRequest IgeorRequest2 = alcocholMeeting.inviteDeputy( Igor );

            Kole.rejectMeetingRequest(KohenRequst);
		  
        }



	  private static void GenerateNotes ( ParlamenterHelperModel _m )
        {
		Notebook notebook = Kole.notebook;

		Note note = notebook.createNote( "laws ideas", "@./resources/notes/Kole/n1" );
		notebook.createNote( "agenda ideas", "@./resources/notes/Kole/n2" );

		note.accessStatus = NoteAccessStatus.Public;
        }

	  private static void GeneratePlenarySessions ( ParlamenterHelperModel _m )
        {
		PlenarySessionSchedule schedule = new PlenarySessionSchedule( Guid.NewGuid() );

		Session = schedule.createSession( new Utils.TimePeriod( new DateTime( 2007, 11, 2, 12, 0, 0 ), new TimeSpan( 3, 0, 0 ) ) );

		  _m.sessionSchedules.Add( schedule );
		
        }

	  private static void GenerateAgendaItems ( ParlamenterHelperModel _m )
        {


		AgendaInclusionRequest request1 = Session.agenda.createAgendaInclusionRequest( NoAlcoholLow, Kole, "That law should be discussed because we propose to change p.1 " );
		AgendaInclusionRequest request2 = Session.agenda.createAgendaInclusionRequest( DrugProhibition, Kole, "That law should be discussed because situation changed" );
		Session.agenda.createAgendaInclusionRequest( CarryingWeaponProhibition, Kohen, "That law should be discussed." );

		
		Session.agenda.acceptInclusionRequest( request1 );
		Session.agenda.acceptInclusionRequest( request2 );	
		
        }

	 private static void GenerateSpeeches ( ParlamenterHelperModel _m )
        {
		Session.changeCurrentBill( NoAlcoholLow );
		SpeechRequest KoleSpeechRequest = Session.createSpeechRequest( Kole );
		SpeechRequest KohenSpeechRequest = Session.createSpeechRequest( Kohen );

		Session.acceptSpeechRequest( KoleSpeechRequest, new DateTime( 2007, 11, 2, 13, 0, 0 ) );
		Session.acceptSpeechRequest( KohenSpeechRequest, new DateTime( 2007, 11, 2, 13, 20, 0 ) );

		Session.changeCurrentBill( DrugProhibition );
		SpeechRequest KoleSpeechRequest2 = Session.createSpeechRequest( Kole );

		Session.refuseSpeechRequest( KoleSpeechRequest2 );
		
        }


	
          private static void GenerateVotings ( ParlamenterHelperModel _m )
        {
		Session.registerDeputy( Kole );
		Session.registerDeputy( Kohen );

		Session.changeCurrentBill( NoAlcoholLow );
		Voting v1 = Session.createVoting( new DateTime( 2007, 11, 2, 12, 30, 0 ) );

		v1.vote( Kole, VoteOption.For );
		v1.vote( Kohen, VoteOption.Against );

		Session.changeCurrentBill( DrugProhibition );
		Voting v2 = Session.createVoting( new DateTime( 2007, 11, 2, 12, 40, 0 ) );

		v2.vote( Kohen, VoteOption.For );
		v2.vote( Kole, VoteOption.For );
		
        }
/*
        private static void GenerateProducts ( ParlamenterHelperModel m )
        {
            // ----

            Mafia = new Product( Guid.NewGuid(), "Mafia" );
            Mafia.ImageUrl = "http://pizzario.com/images/mafia.jpg";

            Mafia.Recipe.Add( new Ingredient( "Mozarella" ) );
            Mafia.Recipe.Add( new Ingredient( "Pineaple" ) );
            Mafia.Recipe.Add( new Ingredient( "Chicken" ) );
            Mafia.Recipe.Add( new Ingredient( "Saliami" ) );
            Mafia.Recipe.Add( new Ingredient( "Tomato" ) );

            Mafia.Prices[ Small  ] = 73.00M;
            Mafia.Prices[ Medium ] = 99.00M;
            Mafia.Prices[ Large  ] = 125.00M;

            // ----

            Georgia = new Product( Guid.NewGuid(), "Georgia" );
            Georgia.ImageUrl = "http://pizzario.com/images/georgia.jpg";

            Georgia.Recipe.Add( new Ingredient( "Kebab" ) );
            Georgia.Recipe.Add( new Ingredient( "Mozarella" ) );
            Georgia.Recipe.Add( new Ingredient( "Eggplant" ) );
            Georgia.Recipe.Add( new Ingredient( "Onion" ) );
            Georgia.Recipe.Add( new Ingredient( "Parsley" ) );
            Georgia.Recipe.Add( new Ingredient( "Tomato" ) );

            Georgia.Prices[ Small  ]  = 81.00M;
            Georgia.Prices[ Medium ] = 103.00M;
            Georgia.Prices[ Large  ] = 137.00M;

            // ----

            Cossack = new Product( Guid.NewGuid(), "Cossack" );
            Cossack.ImageUrl = "http://pizzario.com/images/cosscack.jpg";

            Cossack.Recipe.Add( new Ingredient( "Mozarella" ) );
            Cossack.Recipe.Add( new Ingredient( "Bacon" ) );
            Cossack.Recipe.Add( new Ingredient( "Ham" ) );
            Cossack.Recipe.Add( new Ingredient( "Onion" ) ) ;
            Cossack.Recipe.Add( new Ingredient( "Pickled Cucumber" ) );
            Cossack.Recipe.Add( new Ingredient( "Mushroom" ) );

            Cossack.Prices[ Small ]  = 83.00M;
            Cossack.Prices[ Medium ] = 107.00M;
            Cossack.Prices[ Large  ] = 143.00M;

            // ----

            m.Products.Add( Mafia );
            m.Products.Add( Georgia );
            m.Products.Add( Cossack );
        }


        private static void GenerateCarts ( PizzarioModel m )
        {
            cart = new ShoppingCart( Guid.NewGuid() );
            cart.AddItem( new ProductItem( Mafia,   Large,  1 ) );
            cart.AddItem( new ProductItem( Georgia, Small,  2 ) );
            cart.AddItem( new ProductItem( Cossack, Medium, 1 ) );

            cart.Checkout();

            m.ShoppingCarts.Add( cart );
        }


        private static void GenerateOrders ( PizzarioModel m )
        {
            order = new Order( 
                Guid.NewGuid(), 
                cart,
                new Contact( "Sumskaya 1", "123-45-67" ), 
                DateTime.Now 
            );

            order.SetDiscount( new Discount( 20.00M ) );
            order.Confirm();

            m.Orders.Add( order );
        }


        private static void GenerateCookingAssignments ( PizzarioModel m )
        {
            var orderAssignments = order.GenerateCookingAssignments();
            foreach ( CookingAssignment ca in orderAssignments )
                m.Cookings.Add( ca );
        }


        private static void GenerateDeliveries ( PizzarioModel m )
        {
            m.Deliveries.Add( order.GenerateDelivery( "Ivan Vodilkin" ) );
        }
*/

	private static DeputyAccount Igor, Kohen, Kole;
	private static Law NoAlcoholLow, CarryingWeaponProhibition, DrugProhibition;
	private static PlenarySession Session;
	//private static SpeechRequest IgorSpeechRequest, VasySpeechRequest;
	//private static AgendaInclusionRequest
	//	NoAlcoholLowRequest, CarryingWeaponProhibitionRequest, DrugProhibitionRequest;

    }

}
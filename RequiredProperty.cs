using System;

namespace ParlamenterHelper.Utils
{
    public class RequiredProperty< T > : AbstractProperty
    {
        public T data {
		get
		{
			return m_value;
		}
            	set
		{
	                if ( value == null )
                   		throw new ArgumentNullException( paramName );

                	m_value = value;
            }
        }

        public RequiredProperty ( string _paramName )
        	:	base( _paramName )
        {
        }

        private T m_value;
    }
}
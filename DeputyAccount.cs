using System;
using AssisstantAccounts = ParlamenterHelper.Utils.GuidBasedDictionary< ParlamenterHelper.Model.AssistantAccount >;
using MeetingRequests = System.Collections.Generic.Dictionary< 
		ParlamenterHelper.Model.Meeting
	,	ParlamenterHelper.Model.MeetingRequest
>;
using System.Text;

namespace ParlamenterHelper.Model
{
	public class DeputyAccount
	:	Account
	{
	        public DeputyAccount (
				Guid _id
			,	string _name
			,	string _email
			,	string _password
		)
	            :   base(
					_id
				,	_name
				,	_email
				,	_password
			)
	        {
			m_assistants = new AssisstantAccounts( "assistant" );
			m_meetingRequests = new MeetingRequests();
					
			m_meetingSchedule.data = new MeetingSchedule( Guid.NewGuid(), this );
			m_notebook.data = new Notebook( Guid.NewGuid() );
	        }

		public void assignAssistant( AssistantAccount _assistant )
		{
			m_assistants.addItem( _assistant );
		}

		public void deassignAssistant( AssistantAccount _assistant )
		{
			m_assistants.removeItem( _assistant.id );
		}

		public MeetingSchedule meetingSchedule
		{
			get
			{
				return m_meetingSchedule.data;
			}
		}

		public Notebook notebook
		{
			get
			{
				return m_notebook.data;
			}
		}
        /*
		public void addMeetingRequest( MeetingRequest _meetingRequest )
		{
			Meeting meeting = _meetingRequest.meeting;
			//if ( m_meetingRequests.ContainsKey( _meetingRequest.meeting ) )
			//	throw new ArgumentException();

			m_meetingRequests.Add( meeting, _meetingRequest );
		}
        */
/*
		public Meeting createMeeting( string _subject )
		{
			Meeting newMeeting = new Meeting( _subject, this );

			m_meetings.addItem( newMeeting );
		}
*/

		public void inviteToMeeting( MeetingRequest _meetingRequest )
		{
			m_meetingRequests.Add( _meetingRequest.meeting, _meetingRequest );
		}

		public void disinviteFromMeeting( MeetingRequest _meetingRequest )
		{
			m_meetingRequests.Remove( _meetingRequest.meeting );
		}
		
		public bool hasMeetingRequest( MeetingRequest _meetingRequest )
		{
			return m_meetingRequests.ContainsKey( _meetingRequest.meeting );
		}
		
		public MeetingRequest getMeetingRequest( MeetingRequest _meetingRequest )
		{
			if ( ! hasMeetingRequest( _meetingRequest ) )
				throw new ArgumentException();
			
			return m_meetingRequests[ _meetingRequest.meeting ];
		}
		
		public void acceptMeetingRequest( MeetingRequest _meetingRequest )
		{
            if ( ! hasMeetingRequest( _meetingRequest ) )
            	throw new ArgumentException();

            if (this != _meetingRequest.invitedDeputy)
                throw new ArgumentException();

			meetingSchedule.addMeeting( _meetingRequest.meeting );
			
			_meetingRequest.status = RequestStatus.Accepted;
		}
		
		public void rejectMeetingRequest( MeetingRequest _meetingRequest )
		{
            if (this != _meetingRequest.invitedDeputy)
                throw new ArgumentException();
            if ( ! hasMeetingRequest( _meetingRequest ) )
				throw new ArgumentException();
			
			_meetingRequest.status = RequestStatus.Rejected;
		}
        
		public System.Collections.IEnumerable assistants()
		{
			foreach( var it in m_assistants )
			{
				yield return it;
			}
		}
        
		public System.Collections.IEnumerable meetingRequests()
		{
			foreach( var it in m_meetingRequests )
			{
				yield return it;
			}
		}
	
	        public override string ToString()
	        {
			StringBuilder b = new StringBuilder();
			Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();
				
			b.Append( base.ToString() );
	
			//b.Append( tr.getTabs() );
			//b.AppendFormat(  "ID = {0}\n", id );


			b.Append( tr.getTabs() );
			b.AppendLine( "Assistants:" );
			tr.increaceTabNumber();
			foreach( var it in m_assistants )
			{
				b.Append( tr.getTabs() );
				b.Append( it );
				b.AppendLine();
			}
			tr.decreaceTabNumber();

			b.Append( tr.getTabs() );
			b.AppendLine( "Meeting Requests:" );
            
			tr.increaceTabNumber();
			foreach( var it in m_meetingRequests )
			{
				b.Append( tr.getTabs() );
				b.Append( it );
				b.AppendLine();
			}
			tr.decreaceTabNumber();
            
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Meeting schedule = {0}\n", meetingSchedule );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Notebook = {0}\n", notebook );

			return b.ToString();
	        }
	
	    private readonly AssisstantAccounts m_assistants;
		private Utils.RequiredProperty< MeetingSchedule > m_meetingSchedule
			= new Utils.RequiredProperty< MeetingSchedule >( "meeting schedule" );
		private Utils.RequiredProperty< Notebook > m_notebook
			= new Utils.RequiredProperty< Notebook >( "notebook" );
		private MeetingRequests m_meetingRequests;


    }

}
using System;

namespace ParlamenterHelper.Utils
{
    public class NonEmptyString : AbstractProperty
    {
        public string data
        {
            	get
		{
			return m_data;
		}

		set
		{
			checkValue( value );
			m_data = value;
		}
        }

        public NonEmptyString ( string _paramName )
            :   base( _paramName )
        {
        }

        protected virtual void checkValue ( string _value )
        {
            if ( _value == null )
                throw new ArgumentNullException( paramName );

            if ( _value.Length == 0 )
                throw new ArgumentException(
				"Empty value not allowed"
			,	paramName
		);
        }

        private string m_data;
    }
}
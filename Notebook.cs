using System;
using System.Text;
using Notes = ParlamenterHelper.Utils.GuidBasedDictionary< ParlamenterHelper.Model.Note >;

namespace ParlamenterHelper.Model
{
    public class Notebook
	:	Utils.Entity
    {
        public Notebook ( Guid _id )
            :   base( _id )
        {
		this.m_notes = new Notes( "note" );
        }

	public Note createNote(
			string _subject
		,	string _filePath
	)
	{
		Note newNote = new Note(
					Guid.NewGuid()
				,	_subject
				,	_filePath
		);

		m_notes.addItem( newNote );
		return newNote;
	}

	public void removeNote( Guid _id )
	{
		m_notes.removeItem( _id );
	}

	public void getNote( Guid _id )
	{
		m_notes.getItem( _id );
	}

        public override string ToString()
        {
		StringBuilder b = new StringBuilder();
		Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();

		b.Append( tr.getTabs() );
		b.AppendFormat(  "ID = {0}\n", id );
		b.Append( tr.getTabs() );
		b.AppendLine( "Content:" );
		tr.increaceTabNumber();
		
			foreach( var it in m_notes )
			{
				b.Append( tr.getTabs() );
				b.Append( it );
				b.AppendLine();
			}
		tr.decreaceTabNumber();
            	return b.ToString();

        }

	private readonly Notes m_notes;
    }
}
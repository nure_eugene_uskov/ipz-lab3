using System;
using System.Collections.Generic;
using System.Linq;


namespace ParlamenterHelper.Utils
{
    public abstract class Value< TValue >
    	where TValue : Value< TValue >
    {
        public override bool Equals ( object _other )
        {
            return Equals( _other as TValue);
        }

        public bool Equals ( TValue _other )
        {
            if ( _other == null )
                return false;

            var sequence1 = getAttributesToIncludeInEqualityCheck();
            var sequence2 = _other.getAttributesToIncludeInEqualityCheck();
            return sequence1.SequenceEqual( sequence2 );
        }

        public static bool operator == ( Value< TValue > _left, Value< TValue > _right )
        {
            return Equals( _left, _right );
        }

        public static bool operator != ( Value< TValue > _left, Value< TValue > _right )
        {
            return !( _left == _right );
        }

        public override int GetHashCode ()
        {
            int hash = 17;
            foreach ( var obj in this.getAttributesToIncludeInEqualityCheck() )
                hash = hash * 31 + ( obj == null ? 0 : obj.GetHashCode() );
            return hash;
        }

        protected abstract IEnumerable< object > getAttributesToIncludeInEqualityCheck ();
    }
}
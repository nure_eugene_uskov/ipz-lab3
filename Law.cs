using System;
using System.Text;
namespace ParlamenterHelper.Model
{

	public class Law
		:	Utils.Entity
	{
		public Law(
				Guid _id 
			,	string _name
			,	string _filePath
			,	DateTime _dataTime
		)
			:	base( _id )
		{
			m_name.data = _name;
			m_includedInAgenda = false;
				m_history.data = new LawHistory( Guid.NewGuid() );
			
			m_history.data.addItem(
				new LawHistoryItem(
						_filePath
					,	_dataTime
					,	LawStatus.New
				)
			);
		}

		public string name
		{
			get
			{
				return m_name.data;
			}
		}

		public LawHistory history
		{
			get
			{
				return m_history.data;
			}
		}

		public bool includedInAgenda
		{
			get
			{
				return m_includedInAgenda;
			}
			set
			{
				m_includedInAgenda = value;
			}
		}
		
		public override string ToString()
		{
			StringBuilder b = new StringBuilder();
			Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "ID = {0}\n", id );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Name = {0}\n", name );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "History = {0}\n", history );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Included In Agenda = {0}\n", includedInAgenda );

			return b.ToString();
		}
		
		private bool m_includedInAgenda;

		private readonly Utils.NonEmptyString m_name
            		= new Utils.NonEmptyString( "name" );

		private readonly Utils.RequiredProperty< LawHistory > m_history
            		= new Utils.RequiredProperty< LawHistory >( "law history" );
	}

}
namespace ParlamenterHelper.Model
{
    public enum LawStatus
    {
	        Current
		,	EntryIntoForce
		,	PrezidentSignatureAwaiting
		,	Vetoed
		,	VotedAsWhole
		,	VotedIn1Reading
		,	VotedIn2Reading
		,	VotedIn3Reading
		,	New
    }
}
using System;
using System.IO;
using System.Collections.Generic;

namespace ParlamenterHelper.TestApp
{
    class ModelReporter
    {
        public ModelReporter ( TextWriter output )
        {
            this.output = output;
        }

        public void GenerateReport ( ParlamenterHelperModel _model )
        {
            ReportCollection( "Accounts", _model.accounts );
            ReportCollection( "PlenarySchedules", _model.sessionSchedules );
            ReportCollection( "Laws", _model.laws );
        }

        private void ReportCollection< T > ( string _title, ICollection< T > _items )
        {
            output.WriteLine("==== {0} ==== ", _title );
            output.WriteLine();

            foreach ( var item in _items )
            {
                output.WriteLine( item );
                output.WriteLine();
            }

            output.WriteLine();
        }

        private TextWriter output;
    }
}
using System;
using System.Text;
namespace ParlamenterHelper.Model
{
    public class LawHistoryItem
		:	Utils.Value< LawHistoryItem >
    {
	        public LawHistoryItem (
				string _filePath
			,	DateTime _dateTime
			,	LawStatus _lawStatus
		)
	        {
			this.m_filePath.data = _filePath;
			m_dateTime = _dateTime;
			m_status = _lawStatus;
	        }

		public string filePath
		{
			get
			{
				return m_filePath.data;
			}
		}

		public DateTime dateTime
		{
			get
			{
				return m_dateTime;
			}
		}

		public LawStatus status
		{
			get
			{
				return m_status;
			}
		}
	
	        public override string ToString()
	        {
			StringBuilder b = new StringBuilder();
			Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "File path = {0}\n", filePath );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Date time = {0}\n", dateTime );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Status = {0}\n", status );
			

			return b.ToString();
	        }
		
		protected override System.Collections.Generic.IEnumerable< object > getAttributesToIncludeInEqualityCheck ()
        {
            return new object[] { filePath, dateTime, status };
        }
	
		private readonly Utils.NonEmptyString m_filePath
			= new Utils.NonEmptyString( "file path" );

		private readonly DateTime m_dateTime;

		private readonly LawStatus m_status;
    }
}

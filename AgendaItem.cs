using System;
using System.Text;
namespace ParlamenterHelper.Model
{
	public class AgendaItem
		:	Utils.Value< AgendaItem >
	{
		public AgendaItem(
				Law _law
			,	DeputyAccount _proposingDeputy
			,	string _description
		)
		{
			m_law.data = _law;
			m_proposingDeputy.data = _proposingDeputy;
			m_description.data = _description;
			//m_request.data = _request;
			m_reviewed = false;
		}
		
		public Law law
		{
			get
			{
				return m_law.data;
			}
		}
		
		public DeputyAccount proposingDeputy
		{
			get
			{
				return m_proposingDeputy.data;
			}
		}
		
		public string description
		{
			get
			{
				return m_description.data;
			}
		}

		public bool reviewed
		{
			get
			{
				return m_reviewed;
			}
		}

		public void review()
		{
			m_reviewed = true;
		}
		
		public override string ToString()
		{
			StringBuilder b = new StringBuilder();
			Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Law = {0}\n", law );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Deputy = {0}\n", proposingDeputy );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Description = {0}\n", description );
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "Reviewed = {0}\n", reviewed );

			return b.ToString();
		}
		
		protected override System.Collections.Generic.IEnumerable< object > getAttributesToIncludeInEqualityCheck ()
        {
            return new object[] { law, proposingDeputy, description, reviewed };
        } 

		//private readonly Utils.RequiredProperty< AgendaInclusionRequest > m_request
         //   		= new Utils.RequiredProperty< AgendaInclusionRequest >( "request" );
		
		private readonly Utils.RequiredProperty< Law > m_law
           	= new Utils.RequiredProperty< Law >( "law" );
		
		private readonly Utils.RequiredProperty< DeputyAccount > m_proposingDeputy
           	= new Utils.RequiredProperty< DeputyAccount >( "deputy" );
		
		private readonly Utils.NonEmptyString m_description
			= new Utils.NonEmptyString( "description" );

		private bool m_reviewed;
	}
}
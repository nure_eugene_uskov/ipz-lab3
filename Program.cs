using System;

#pragma comment(linker, "/STACK:32000000")
#pragma comment(linker, "/HEAP:32000000")

namespace ParlamenterHelper.TestApp
{
    public class Program
    {
        public static void Main ()
        {
            try
            {
                ParlamenterHelperModel model = TestModelGenerator.GenerateTestData();

               ModelReporter reporter = new ModelReporter( Console.Out );
                reporter.GenerateReport( model );

                Console.ReadLine();
            }
            catch ( Exception e )
            {
                Console.WriteLine( e.GetType().FullName );
                Console.WriteLine( e.Message );
                Console.WriteLine( e.StackTrace );
            }
        }
    }
}
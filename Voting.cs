using System;
using System.Text;
namespace ParlamenterHelper.Model
{
	public class Voting
		:	Utils.Entity
	{
		public Voting(
				Guid _id
			,	PlenarySession _plenarySession
			,	Law _law
			,	DateTime _dateTime
		//	,	int _totalDeputiesCount
		//	,	int _registeredDeputiesCount
		)
			:	base ( _id )
		{
			//m_plenarySession = _plenarySession.data;
			//m_registeredDeputies = new GuidBasedDictionary< DeputyAccount >( "deputy" );
			//m_totalDeputiesCount.data = _totalDeputiesCount;
			//m_registeredDeputiesCount.data = _registeredDeputiesCount;
			m_plenarySession.data = _plenarySession;
			m_votingResults = new System.Collections.Generic.Dictionary< DeputyAccount, VoteOption >();
			m_dateTime = _dateTime;
			m_law.data = _law;
			//m_closed = false;
		}

		public DateTime dateTime
		{
			get
			{
				return m_dateTime;
			}
		}

		public Law law
		{
			get
			{
				return m_law.data;
			}
		}

		public PlenarySession plenarySession
		{
			get
			{
				return m_plenarySession.data;
			}
		}
/*
		public int totalDeputiesCount
		{
			get
			{
				return m_totalDeputiesCount.data;
			}
		}

		public int registeredDeputiesCount
		{
			get
			{
				return m_registeredDeputiesCount.data;
			}
		}
*/
		public int getVoteResult( VoteOption _option )
		{
			switch( _option )
			{
				case VoteOption.Absent:
				{
					return plenarySession.totalDeputiesCount - plenarySession.registeredDeputiesCount;
				}
				case VoteOption.DontVote:
				{
					return plenarySession.registeredDeputiesCount - m_votingResults.Count;
				}

				default:
				{
					int result = 0;
					foreach( var it in m_votingResults )
					{
						if ( it.Value == _option )
							++result;
					}
					return result;
				}
			}
		}

		public void vote( DeputyAccount _deputy, VoteOption _option )
		{
			if ( !plenarySession.hasDeputy( _deputy ) )
				throw new ArgumentException();

			if ( !m_votingResults.ContainsKey( _deputy ) )
				m_votingResults.Add( _deputy, _option );
			else
				m_votingResults[ _deputy ] = _option;
		}

		public override string ToString()
		{
			StringBuilder b = new StringBuilder();
			Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "ID = {0}\n", id );

			b.Append( tr.getTabs() );
			b.AppendLine( "Voting results:" );
			tr.increaceTabNumber();
			foreach( var it in m_votingResults )
			{
				b.Append( tr.getTabs() );
				b.Append( it );
				b.AppendLine();
			}
			tr.decreaceTabNumber();

			b.Append( tr.getTabs() );
			b.AppendFormat(  "Plenary session id = {0}\n", plenarySession.id );

			b.Append( tr.getTabs() );
			b.AppendFormat(  "Law = {0}\n", law );

			b.Append( tr.getTabs() );
			b.AppendFormat(  "Date time = {0}\n", m_dateTime );

			return b.ToString();
			
		}

		//private readonly Utils.RequiredProperty< PlenarySession > m_plenarySession
            	//	= new Utils.RequiredProperty< PlenarySession >( "plenary session" );

		
/*
        	private Utils.RangeProperty< int > m_totalDeputiesCount = 
           		new Utils.RangeProperty< int >( "total deputies count", 0, false, int.MaxValue, true );

        	private Utils.RangeProperty< int > m_registeredDeputiesCount = 
           		new Utils.RangeProperty< int >( "registered deputies count", 0, true, int.MaxValue, true );
*/

		private readonly Utils.RequiredProperty< PlenarySession > m_plenarySession
            		= new Utils.RequiredProperty< PlenarySession >( "plenary session" );

		private System.Collections.Generic.Dictionary< DeputyAccount, VoteOption > m_votingResults;

		private readonly Utils.RequiredProperty< Law > m_law
            		= new Utils.RequiredProperty< Law >( "law" );

		private DateTime m_dateTime;
	}

}
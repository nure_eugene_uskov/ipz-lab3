using System;
using System.Text;
namespace ParlamenterHelper.Utils
{
public class TabsReporter
    {

        private static TabsReporter m_tabsReporter = new TabsReporter();
  
        public static TabsReporter getTabsReporter()
	{
		return m_tabsReporter;
	}

	public void increaceTabNumber()
	{
		++m_tabsNumber;
	}

	public void decreaceTabNumber()
	{
		--m_tabsNumber;
	}

	public string getTabs()
	{
		StringBuilder b = new StringBuilder();

		for ( int i = 0; i < m_tabsNumber; ++i )
		{
			b.Append( "\t" );
		}

		return b.ToString();
	}

	private TabsReporter()
	{
		m_tabsNumber = 0;
	}

	private int m_tabsNumber;
}	
}
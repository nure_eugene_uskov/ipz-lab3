using System;
using System.Text;
using MeetingRequests = System.Collections.Generic.Dictionary<
		ParlamenterHelper.Model.DeputyAccount
	,	ParlamenterHelper.Model.MeetingRequest
>;

namespace ParlamenterHelper.Model
{
public class Meeting
	:	Utils.Entity
//	,	System.Collections.IEnumerable
	,	Utils.ILastingEvent
{
	public Meeting(
			Guid _id
		,	string _subject
		,	DeputyAccount _initiator
		,	Utils.TimePeriod _timePeriod
	)
		:	base( _id )
	{
		m_subject.data = _subject;
		m_initiator.data = _initiator;
		m_timePeriod = _timePeriod;
		m_requests = new MeetingRequests();
	}

	public string subject
	{
		get
		{
			return m_subject.data;
		}
	}

	public DeputyAccount initiator
	{
		get
		{
			return m_initiator.data;
		}
	}

	public Utils.TimePeriod timePeriod
	{
		get
		{
			return m_timePeriod;
		}
	}

	public MeetingRequest inviteDeputy( DeputyAccount _deputy )
	{
		MeetingRequest newRequest = new MeetingRequest(
				Guid.NewGuid()
			,	this
			,	_deputy
		);
		///_deputy.addMeetingRequest( newRequest );
		m_requests.Add( _deputy, newRequest );
		return newRequest;
	}

	public void disinviteDeputy( DeputyAccount _deputy )
	{
		MeetingRequest request = m_requests[ _deputy ];
		if ( _deputy.hasMeetingRequest( request ) )
			_deputy.disinviteFromMeeting( request );

		m_requests.Remove( _deputy );
	}

	public System.Collections.IEnumerable meetingRequests()
	{
		foreach( var it in m_requests )
		{
			yield return it;
		}
	}

/*
	public System.Collections.IEnumerator GetEnumerator()
	{
		foreach ( var it in m_requests )
		yield return it;
	}
*/
	public override string ToString()
	{
		StringBuilder b = new StringBuilder();
		Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();

		b.Append( tr.getTabs() );
		b.AppendFormat(  "ID = {0}\n", id );


		b.Append( tr.getTabs() );
		b.AppendLine( "Meeting Requests:" );
		tr.increaceTabNumber();
		foreach( var it in m_requests )
		{
			b.Append( it );
			b.AppendLine();
		}
		tr.decreaceTabNumber();

		b.Append( tr.getTabs() );
		b.AppendFormat(  "Subject = {0}\n", subject );

		b.Append( tr.getTabs() );
		b.AppendFormat(  "Initiator name = {0}\n", initiator.name );

		b.Append( tr.getTabs() );
		b.AppendFormat(  "Time period = {0}\n", timePeriod );

		return b.ToString();
	}

	private readonly Utils.NonEmptyString m_subject
            	= new Utils.NonEmptyString( "sybject" );

	private readonly Utils.RequiredProperty< DeputyAccount > m_initiator
            	= new Utils.RequiredProperty< DeputyAccount >( "deputy" );

	private readonly MeetingRequests m_requests;

	private readonly Utils.TimePeriod m_timePeriod;

}

}
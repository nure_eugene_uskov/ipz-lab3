using System;

namespace ParlamenterHelper.Utils
{
    public class RangeProperty< T >
	:	AbstractProperty
		where T : IComparable< T >
    {
        public T data
        {
           	get
		{
			return m_currentValue;
		}

		set
		{
			checkValue( value );
                	m_currentValue = value;
		}
        }

        public RangeProperty (
			string _paramName
		,	T _minValue
		,	bool _includingMin
		,	T _maxValue
		,	bool _includingMax
	)
            :   base( _paramName )
        {
            this.m_minValue = _minValue;
            this.m_includingMin = _includingMin;
            this.m_maxValue = _maxValue;
            this.m_includingMax = _includingMax;
        }


        protected void checkValue ( T _value )
        {
            if ( m_includingMin && _value.CompareTo( m_minValue ) < 0 ||
                ! m_includingMin && _value.CompareTo( m_minValue ) <= 0 )
            {
                throw new ArgumentException( "Minimal range violated", paramName );
            }

            if ( m_includingMax && _value.CompareTo( m_maxValue ) > 0 ||
                 ! m_includingMax && _value.CompareTo( m_maxValue ) >= 0 )
            {
                throw new ArgumentException( "Maximal range violated", paramName );
            }
        }

        private T m_currentValue;
        private readonly T m_minValue;
	private readonly T m_maxValue;
        private readonly bool m_includingMin;
	private readonly bool m_includingMax;
    }
}
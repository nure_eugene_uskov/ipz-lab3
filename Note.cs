using System;
using System.Text;
namespace ParlamenterHelper.Model
{
    public class Note : Utils.Entity
    {
		public string subject
		{
        	get
			{
				return m_subject.data;
			}
        	set
			{
				m_subject.data = value;
			}
        }

        public string filePath
		{
        	 get
			{
				return m_filePath.data;
			}
        }

		public NoteAccessStatus accessStatus
		{
			get
			{
				return m_accessStatus;
			}
	
			set
			{
				m_accessStatus = value;
			}
		}

        public Note (
				Guid _id
			,	string _subject
			,	string _filePath
		)
            :   base( _id )
        {
            this.m_subject.data = _subject;
            this.m_filePath.data = _filePath;
        }

        public override string ToString()
        {
			
		StringBuilder b = new StringBuilder();
		Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();

		b.Append( tr.getTabs() );
		b.AppendFormat(  "ID = {0}\n", id );

		b.Append( tr.getTabs() );
		b.AppendFormat(  "Subject = {0}\n", subject );

		b.Append( tr.getTabs() );
		b.AppendFormat(  "File path = {0}\n", filePath );

		b.Append( tr.getTabs() );
		b.AppendFormat(  "AccessStatus = {0}\n", accessStatus );

		return b.ToString();
        }

        private Utils.NonEmptyString m_subject = new Utils.NonEmptyString( "subject" );
		private Utils.NonEmptyString m_filePath = new Utils.NonEmptyString( "filePath" );
		private NoteAccessStatus m_accessStatus = NoteAccessStatus.Private;

    }
}
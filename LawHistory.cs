using System;
using System.Text;
namespace ParlamenterHelper.Model
{
    public class LawHistory
	:	Utils.Entity
    {
	        public LawHistory ( Guid _id )
	            :   base( _id )
	        {
			this.m_items = new System.Collections.Generic.List< LawHistoryItem >();
	        }

		public void addItem( LawHistoryItem _item )
		{
			if ( !checkItem( _item ) )
				throw new Exception();

			m_items.Add( _item );
		}

		public LawHistoryItem getLastVersion()
		{
			if ( m_items.Count == 0 )
				return null;

			return m_items[ m_items.Count - 1 ];
		}

		public LawHistoryItem getItem( int _index )
		{
			return m_items[ _index ];
		}
	
	        public override string ToString()
	        {		
			StringBuilder b = new StringBuilder();
			Utils.TabsReporter tr = Utils.TabsReporter.getTabsReporter();
	
			b.Append( tr.getTabs() );
			b.AppendFormat(  "ID = {0}\n", id );
			
			b.Append( tr.getTabs() );
			b.AppendLine( "Content:" );
			tr.increaceTabNumber();
			foreach( var item in m_items )
			{
				b.Append( item );
				b.AppendLine();
			}

			tr.decreaceTabNumber();

			return b.ToString();
	        }

		private bool checkItem( LawHistoryItem _item )
		{
			if ( m_items.Count == 0 )
				return true;

			if ( getLastVersion().dateTime >= _item.dateTime )
				return false;
			
			return true;
		}

		private readonly System.Collections.Generic.List< LawHistoryItem >
			m_items;
    }
}
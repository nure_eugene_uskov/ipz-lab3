using ParlamenterHelper.Model;

using System;
using System.Collections.Generic;

namespace ParlamenterHelper.TestApp
{
    class ParlamenterHelperModel
    {
        public ICollection< Account > accounts { get; private set; }

        public ICollection< PlenarySessionSchedule > sessionSchedules { get; private set; }

        public ICollection< Law > laws { get; private set; }


        public ParlamenterHelperModel ()
        {
            this.accounts      = new List< Account >();
            this.sessionSchedules  = new List< PlenarySessionSchedule >();
            this.laws  = new List< Law >();
        }

    }
}